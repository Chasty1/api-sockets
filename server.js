const express = require("express");
const http = require("http");
const socketio = require("socket.io");
const { ExpressPeerServer } = require("peer");
const mongoose = require("mongoose");
const config = require("config");
const Active = require("./schema/Active");

const app = express();

const server = http.createServer(app);
const io = socketio(server).sockets;

console.log(io)

//** Peer Server */
const customGenerationFunction = () =>
  (Math.random().toString(36) + "0000000000000000000").substr(2, 16);

const peerServer = ExpressPeerServer(server, {
  debug: true,
  path: "/",
  generateClientId: customGenerationFunction,
});

app.use("/mypeer", peerServer);

app.get("/", (req, res) => {
  res.send({some: 'json'})
})

peerServer.on('connection', function(id) {
    console.log(id)
  console.log(server._clients)
});

//** Config */
const db = config.get("mongoURI");

mongoose.connect(db, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false
}).then(()=> console.log("Mongodb connected"))
.catch((err)=>console.log(err))

//* Websocket *//
io.on("connection", function (socket) {
  console.log('connected ' + socket.id)
  socket.on('join-general-room', (roomID)=>{
    console.log(roomID)
    socket.join(roomID)
  })

  socket.on('user-exists', ({user, socketID})=>{
    //Check if the new user exits in active chat
    Active.findOne({email: user.email}).then((user)=>{
      // Emit found to last connected user
      console.log('new user exists in active chat ', user)
      io.in(socketID).emit('user-found', user)
      
    })

    //Update user if found
    socket.on('update-user', ({user, socketID, allUserRoomID})=>{
      socket.join(allUserRoomID)

      //Find user and update the socket id
      Active.findOneAndUpdate(
       {email: user.email},
        {$set: {socketID}},
        {new: true},
        (err, doc)=>{
          if(doc) {
            console.log(user)
            console.log("send active users")
            //send active users to the last connected user
            Active.find({}).then(allUsers => {
              const otherUsers = allUsers.filter(({ email: otherEmails }) => otherEmails !== user.email);
              console.log({otherUsers})

              io.in(socketID).emit('activeUsers', otherUsers)

            })
            
          }
        }
      )

      //Notify other user about updated or joined user

      socket
        .to(allUserRoomID)
        .broadcast.emit('new-user-join', [{ ...user, socketID }])

    })

    socket.on('user-join', ({ allUserRoomID, user, socketID })=>{
      socket.join(allUserRoomID)

      //Store new user in active chats
      const active = new Active({...user, socketID})

      // Find the documents || add the document
      Active.findOne({ email: user.email }).then(user => {
        if(!user) {
          active.save().then(({ email })=> {
            Active.find({}).then(users => {
              const otherUsers = users.filter(({email: otherEmails})=> otherEmails !== email)

              //send others to new connected user
              io.in(socketID).emit('activeUsers', otherUsers)

            })
          })
        } else {
          // Emit to all other users the last joined user
          socket.to(allUserRoomID).broadcast.emit('new-user-join', user)
        }

      })
    })

  })

  //Listen for peer connections
  socket.on('join-stream-room', ({roomID, peerID, socketID, user, myStream})=>{
    console.log("Lets join to room " + roomID + "  peerID " + peerID)
    console.log(myStream)
    socket.join(roomID)

    //Emit to other users in same room
    socket.to(roomID).emit('user-connected', {
      peerID, user, roomID, socketID, myStream
    })
  })

});

const port = process.env.PORT || 5000;
server.listen(port, () => console.log(`Server started on port ${port}`));
